package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"

	"github.com/gofiber/fiber/v2"
	"github.com/joho/godotenv"
)

func main() {
	err := godotenv.Load()
	if err != nil {
		panic(err)
	}
	client, ctx, cancel, err := ConnectToMongo(os.Getenv("MONGODB_CONNECTION_URL"))

	if err != nil {
		panic(err)
	}

	if err = Ping(client, ctx); err != nil {
		fmt.Println(err.Error())
		panic(err)
	}

	app := fiber.New()

	app.Get("/", func(c *fiber.Ctx) error {
		return c.SendString("Hello, World!")
	})
	app.Post("reminders", func(c *fiber.Ctx) error {
		return CreateReminder(c, client, ctx)
	})
	app.Put("reminders/:id", func(c *fiber.Ctx) error {
		return UpdateReminder(c, client, ctx)
	})
	app.Delete("reminders/:id", func(c *fiber.Ctx) error {
		return DeleteReminder(c, client, ctx)
	})
	app.Get("reminders/:id", func(c *fiber.Ctx) error {
		return GetReminder(c, client, ctx)
	})
	app.Get("reminders", func(c *fiber.Ctx) error {
		return GetReminders(c, client, ctx)
	})

	// Listen from a different goroutine
	go func() {
		if err := app.Listen(":3000"); err != nil {
			log.Panic(err)
		}
	}()

	c := make(chan os.Signal, 1)                    // Create channel to signify a signal being sent
	signal.Notify(c, os.Interrupt, syscall.SIGTERM) // When an interrupt or termination signal is sent, notify the channel

	<-c // This blocks the main thread until an interrupt is received
	fmt.Println("Gracefully shutting down...")
	_ = app.Shutdown()

	fmt.Println("Running cleanup tasks...")

	// Your cleanup tasks go here
	CloseMongo(client, ctx, cancel)
	// redisConn.Close()
	fmt.Println("Fiber was successful shutdown.")

}
