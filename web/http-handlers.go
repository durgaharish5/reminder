package main

import (
	"context"
	"os"
	"time"

	"github.com/araddon/dateparse"
	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"gopkg.in/mgo.v2/bson"
)

type Reminder struct {
	ID            uuid.UUID   `json:"id"`
	Name          string      `json:"name"`
	ReminderTimes []string    `json:"reminder_times"`
	Enabled       bool        `json:"enabled"`
	Data          interface{} `json:"data"`
	Webhook       string      `json:"webhook"`
}

func CreateReminder(c *fiber.Ctx, db *mongo.Client, ctx context.Context) error {
	// Unmarshal the body into Reminder struct
	var reminder Reminder
	if err := c.BodyParser(&reminder); err != nil {
		return c.Status(500).SendString(err.Error())
	}
	parsedTimes := []time.Time{}
	for _, t := range reminder.ReminderTimes {
		parsedTime, err := dateparse.ParseLocal(t)
		if err != nil {
			return c.Status(500).SendString(err.Error())
		}
		parsedTimes = append(parsedTimes, parsedTime)
	}

	document := bson.M{
		"name":           reminder.Name,
		"reminder_times": parsedTimes,
		"enabled":        reminder.Enabled,
		"created_at":     time.Now(),
		"data":           reminder.Data,
		"webhook":        reminder.Webhook,
		"is_processing":  false,
	}
	// Insert the reminder into the database
	collection := db.Database(os.Getenv("MONGODB_DATABASE_NAME")).Collection("reminders")
	InsertOne, err := collection.InsertOne(context.Background(), document)
	if err != nil {
		return c.Status(500).SendString(err.Error())
	}
	document["id"] = InsertOne.InsertedID
	return c.Status(201).JSON(document)
}

func UpdateReminder(c *fiber.Ctx, db *mongo.Client, ctx context.Context) error {
	// Unmarshal the body into Reminder struct
	var reminder Reminder
	if err := c.BodyParser(&reminder); err != nil {
		return c.Status(500).SendString(err.Error())
	}
	parsedTimes := []time.Time{}
	for _, t := range reminder.ReminderTimes {
		parsedTime, err := dateparse.ParseLocal(t)
		if err != nil {
			return c.Status(500).SendString(err.Error())
		}
		parsedTimes = append(parsedTimes, parsedTime)
	}

	document := bson.M{
		"name":           reminder.Name,
		"reminder_times": parsedTimes,
		"enabled":        reminder.Enabled,
		"updated_at":     time.Now(),
		"data":           reminder.Data,
		"webhook":        reminder.Webhook,
	}
	// Update the reminder in the database
	collection := db.Database(os.Getenv("MONGODB_DATABASE_NAME")).Collection("reminders")
	id, err := primitive.ObjectIDFromHex(c.Params("id"))
	if err != nil {
		return c.Status(500).SendString("Error updating reminder")
	}
	filter := bson.M{"_id": id}
	UpdateOne, err := collection.UpdateOne(context.Background(), filter, bson.M{"$set": document})
	if err != nil {
		return c.Status(500).SendString("Error updating reminder")
	}
	return c.Status(200).JSON(UpdateOne)
}

func DeleteReminder(c *fiber.Ctx, db *mongo.Client, ctx context.Context) error {
	// Delete the reminder from the database
	collection := db.Database(os.Getenv("MONGODB_DATABASE_NAME")).Collection("reminders")
	id, err := primitive.ObjectIDFromHex(c.Params("id"))
	if err != nil {
		return c.Status(500).SendString("Error deleting reminder")
	}
	filter := bson.M{"_id": id}
	_, err = collection.DeleteOne(context.Background(), filter)
	if err != nil {
		return c.Status(500).SendString("Error deleting reminder")
	}
	//no body
	return c.Status(200).JSON(bson.M{})
}

func GetReminder(c *fiber.Ctx, db *mongo.Client, ctx context.Context) error {
	// Get the reminder from the database
	collection := db.Database(os.Getenv("MONGODB_DATABASE_NAME")).Collection("reminders")
	id, err := primitive.ObjectIDFromHex(c.Params("id"))
	if err != nil {
		return c.Status(500).SendString(err.Error())
	}
	filter := bson.M{"_id": id}
	var result bson.M
	err = collection.FindOne(context.Background(), filter).Decode(&result)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			// This error means your query did not match any documents.
			return c.Status(404).SendString("Not found")
		}
		return c.Status(500).SendString(err.Error())
	}

	return c.Status(200).JSON(result)
}

func GetReminders(c *fiber.Ctx, db *mongo.Client, ctx context.Context) error {
	// Get all reminders from the database
	collection := db.Database(os.Getenv("MONGODB_DATABASE_NAME")).Collection("reminders")
	cursor, err := collection.Find(context.Background(), bson.M{})
	if err != nil {
		return c.Status(500).SendString(err.Error())
	}
	var results []bson.M
	if err = cursor.All(context.TODO(), &results); err != nil {
		panic(err)
	}
	return c.Status(200).JSON(results)
}
