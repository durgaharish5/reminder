package main

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/go-co-op/gocron"
	"github.com/joho/godotenv"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

func fetchRecords(client *mongo.Client, ctx context.Context, cancel context.CancelFunc) {
	collection := client.Database(os.Getenv("MONGODB_DATABASE_NAME")).Collection("reminders")
	t := time.Now()
	lowerTime := time.Date(t.Year(), t.Month(), t.Day(), t.Hour(), t.Minute(), 0, 0, time.UTC)
	upperTime := time.Date(t.Year(), t.Month(), t.Day(), t.Hour(), t.Minute(), 59, 0, time.UTC)
	fmt.Println("Fetching At: ", lowerTime, upperTime)

	cursor, err := collection.Find(context.Background(), bson.D{{"reminder_times", bson.D{{"$gte", lowerTime}, {"$lte", upperTime}}}})
	if err != nil {
		fmt.Println(err)
		return
	}
	var results []bson.M
	if err = cursor.All(context.TODO(), &results); err != nil {
		fmt.Println(err)
	}
	fmt.Println(results)
}

func main() {
	err := godotenv.Load()
	if err != nil {
		panic(err)
	}
	client, ctx, cancel, err := ConnectToMongo(os.Getenv("MONGODB_CONNECTION_URL"))

	if err != nil {
		panic(err)
	}

	if err = Ping(client, ctx); err != nil {
		fmt.Println(err.Error())
		panic(err)
	}

	s := gocron.NewScheduler(time.UTC)

	s.Every(1).Minutes().Do(func() {
		fetchRecords(client, ctx, cancel)
	})
	s.StartAsync()
	c := make(chan os.Signal, 1)                    // Create channel to signify a signal being sent
	signal.Notify(c, os.Interrupt, syscall.SIGTERM) // When an interrupt or termination signal is sent, notify the channel

	<-c // This blocks the main thread until an interrupt is received
	fmt.Println("Gracefully shutting down...")
	CloseMongo(client, ctx, cancel)
}
